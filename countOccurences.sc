//Referencing the code review session on Thursday August 1, 2013

val tokens = List("a", "b" ,"cat", "dog", "a", "dog", "dog", "never", "look", "I", "i", "test")


tokens.count(_ == "b") //this would return the number of times b is in the list (1)

def countListOccurences(list: List[String]): Map[String, Int] = {
  var seen: Map [String, Int] = Map()
  list.foreach((i: String) => seen += (i -> list.count(_ == i)))
  seen
}

countListOccurences(tokens)



//A more compact version (no need to make the map a var)
//I know this can be a lot more compact still.
//This can probably be all done in one function call.
def countListOccurencesCompact(list: List[String]): Map [String, Int] = {
  list.map((i: String) => i -> list.count(_ == i)).toMap
}
countListOccurencesCompact(tokens)

//Note: the second function is more compact because foreach is like map

// but foreach does not return anything and Map returns a list
// Whenever I make a variable and incrementally add to it I ask myself:
//  "Can I get all of this at once instead?"








